package com.example.calendar;

public class Events {
    String Event,Time,Date,Month,Year;

    public Events(String event, String time, String date, String month, String year) {
        Event = event;
        Time = time;
        Date = date;
        Month = month;
        Year = year;
    }

    public String getEvent() {
        return Event;
    }

    public String getTime() {
        return Time;
    }

    public String getDate() {
        return Date;
    }

    public String getMonth() {
        return Month;
    }

    public String getYear() {
        return Year;
    }

    public void setEvent(String event) {
        Event = event;
    }

    public void setTime(String time) {
        Time = time;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public void setYear(String year) {
        Year = year;
    }

}
