package com.example.calendar;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyGridAdapter extends ArrayAdapter {

    List<Date> dates;
    Calendar currentDate;
    List<Events> events;
    LayoutInflater inflater;


    public MyGridAdapter(Context context,List<Date> dates,Calendar currentDate,List<Events> events) {
        super(context,R.layout.single_cell_layout);

        this.dates=dates;
        this.currentDate=currentDate;
        this.events=events;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date monthDate=dates.get(position);
        Calendar dateCalendar=Calendar.getInstance();
        dateCalendar.setTime(monthDate);
        int DayNo=dateCalendar.get(Calendar.DAY_OF_MONTH);
        int disaplyMonth=dateCalendar.get(Calendar.MONTH)+1;
        int disaplyYear=dateCalendar.get(Calendar.YEAR);
        int currentMonth=currentDate.get(Calendar.MONTH)+1;
        int currentYear=currentDate.get(Calendar.YEAR);


        View view = convertView;
        if(view==null) {
            view = inflater.inflate(R.layout.single_cell_layout,parent,false);
        }

        if(disaplyMonth == currentMonth && disaplyYear == currentYear)
        {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));

        }
        else
        {
            view.setBackgroundColor(Color.parseColor("#cccccc"));

        }

        TextView DayNumber=view.findViewById(R.id.cal_day);
        TextView eventNumber=view.findViewById(R.id.event_id);
        DayNumber.setText(String.valueOf(DayNo));
        Calendar eventCalendar=Calendar.getInstance();
        ArrayList<String> arrayList=new ArrayList<>();

        for(int i=0; i<events.size();i++)
        {
            eventCalendar.setTime(convertStringToDate(events.get(i).getDate()));

            if(DayNo == eventCalendar.get(Calendar.DAY_OF_MONTH) && disaplyMonth ==eventCalendar.get(Calendar.MONTH)+1 && disaplyMonth ==eventCalendar.get(Calendar.YEAR))
            {
                arrayList.add(events.get(i).getEvent());
                eventNumber.setText(arrayList.size()+"Events");

            }
        }

        return view;
    }

    private Date convertStringToDate(String eventDate)
    {
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date=null;
        try
        {
            date=format.parse(eventDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public int getPosition(Object item) {
        return dates.indexOf(item);
    }

    @Override
    public Object getItem(int position) {
        return dates.get(position);
    }
}
