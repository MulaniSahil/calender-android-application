package com.example.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabase extends SQLiteOpenHelper {

    private static final String Create_Event_Table="create table "+DBStructure.EventTableName+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, "+DBStructure.Event+"TEXT, "+DBStructure.Time+"TEXT, "+DBStructure.Date+"TEXT, "+DBStructure.Month+"TEXT, "+DBStructure.Year+"TEXT) ";
    private static final String DROP_EVENT_TABLE="DROP TABLE IF EXISTS "+DBStructure.EventTableName;

    public MyDatabase(Context context) {
        super(context,DBStructure.DBName,null,DBStructure.DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_Event_Table);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_EVENT_TABLE);
        onCreate(db);
    }

    public void saveEvent(String event,String time,String date,String month,String year,SQLiteDatabase database)
    {
        ContentValues contentValues=new ContentValues();
        contentValues.put(DBStructure.Event,event);
        contentValues.put(DBStructure.Time,time);
        contentValues.put(DBStructure.Date,date);
        contentValues.put(DBStructure.Month,month);
        contentValues.put(DBStructure.Year,year);

        database.insert(DBStructure.EventTableName,null,contentValues);
    }

    public Cursor getEvent(String date, SQLiteDatabase database)
    {
        String [] Projection ={DBStructure.Event,DBStructure.Time,DBStructure.Date,DBStructure.Month,DBStructure.Year};
        String Selection=DBStructure.Date+"=?";
        String [] SelectionArgs={date};

        return database.query(DBStructure.EventTableName,Projection,Selection,SelectionArgs,null,null,null);
    }

    public Cursor getEventPerMonth(String month, String year, SQLiteDatabase database)
    {
        String [] Projections={DBStructure.Event,DBStructure.Time,DBStructure.Date,DBStructure.Month,DBStructure.Year};
        String Selection=DBStructure.Month+"=? and "+DBStructure.Year+"=?";
        String [] SelectionArgs={month,year};

        Cursor cursor=database.query(DBStructure.EventTableName,Projections,Selection,SelectionArgs,null,null,null);
        return cursor;
    }

    public void deleteEvent(String event,String date,String time,SQLiteDatabase database)
    {
        String selection =DBStructure.Event+"=? and " +DBStructure.Date+"=? and "+DBStructure.Time+"=?";
        String[] selectionArg={event,date,time};
        database.delete(DBStructure.EventTableName,selection,selectionArg);

    }
}
