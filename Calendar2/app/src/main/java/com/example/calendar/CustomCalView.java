package com.example.calendar;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class CustomCalView extends LinearLayout {

    Button prevButton,nextButton;
    TextView currentDate;
    GridView gridView;
    private static final int max_cal_days=42;
    Calendar calendar= Calendar.getInstance(Locale.ENGLISH);

    MyGridAdapter myGridAdapter;
    AlertDialog alertDialog;
    Context context;
    SimpleDateFormat dateFormat=new SimpleDateFormat("MMMM yyyy",Locale.ENGLISH);
    SimpleDateFormat monthFormat=new SimpleDateFormat("MMMM",Locale.ENGLISH);
    SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy",Locale.ENGLISH);
    SimpleDateFormat evenDateFormat=new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);

    MyDatabase myDatabase;

    List<Date> dates=new ArrayList<>();
    List<Events> eventList=new ArrayList<>();

    public CustomCalView(Context context) {
        super(context);
    }

    public CustomCalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomCalView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        InitialLayout();
        setCalendar();

        prevButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH,-1);
                setCalendar();
            }
        });

        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH,1);
                setCalendar();
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context);
                builder.setCancelable(true);
                final View addView=LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_event,null);
                final EditText eventName = addView.findViewById(R.id.eventName);
                final TextView eventTime = addView.findViewById(R.id.eventTime);
                Button setTime = addView.findViewById(R.id.setEventTime);
                Button addEvent = addView.findViewById(R.id.addEvent);

                setTime.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calendar = Calendar.getInstance();
                        int hours= calendar.get(Calendar.HOUR_OF_DAY);
                        int minute=calendar.get(Calendar.MINUTE);
                        TimePickerDialog timePickerDialog=new TimePickerDialog(addView.getContext(), R.style.Theme_AppCompat_Dialog, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Calendar c=Calendar.getInstance();
                                c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                                c.set(Calendar.MINUTE,minute);
                                c.setTimeZone(TimeZone.getDefault());
                                SimpleDateFormat hformat = new SimpleDateFormat("K:mm a",Locale.ENGLISH);
                                String event_Time = hformat.format(c.getTime());
                                eventTime.setText(event_Time);
                            }
                        },hours,minute,false);
                        timePickerDialog.show();
                    }
                });

                final String date =evenDateFormat.format(dates.get(position));
                final String month =monthFormat.format(dates.get(position));
                final String year =yearFormat.format(dates.get(position));

                addEvent.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        saveEvent(eventName.getText().toString(),eventTime.getText().toString(),date,month,year);
                        setCalendar();
                        alertDialog.dismiss();
                    }
                });
                builder.setView(addView);
                alertDialog =builder.create();
                alertDialog.show();
            }
        });


        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String date=evenDateFormat.format(dates.get(position));
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setCancelable(true);
                View showView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_event_layout,null);

                RecyclerView recyclerView=showView.findViewById(R.id.eventRv);
                RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(showView.getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                EventRecyclerAdapter eventRecyclerAdapter=new EventRecyclerAdapter(showView.getContext(), CollectEventByDate(date));
                recyclerView.setAdapter(eventRecyclerAdapter);
                eventRecyclerAdapter.notifyDataSetChanged();

                builder.setView(showView);
                alertDialog=builder.create();
                alertDialog.show();
                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        setCalendar();
                    }
                });

                return true;
            }
        });

    }

    private ArrayList<Events> CollectEventByDate(String date)
    {
        ArrayList<Events> arrayList=new ArrayList<>();
        myDatabase=new MyDatabase(context);
        SQLiteDatabase database=myDatabase.getReadableDatabase();
        Cursor cursor=myDatabase.getEvent(date,database);

        while(cursor.moveToNext())
        {
            String event= cursor.getString(cursor.getColumnIndex(DBStructure.Event));
            String time= cursor.getString(cursor.getColumnIndex(DBStructure.Time));
            String Date= cursor.getString(cursor.getColumnIndex(DBStructure.Date));
            String Month= cursor.getString(cursor.getColumnIndex(DBStructure.Month));
            String Year= cursor.getString(cursor.getColumnIndex(DBStructure.Year));
            Events events=new Events(event,time,Date,Month,Year);
            arrayList.add(events);

        }
        cursor.close();
        myDatabase.close();
        return arrayList;
    }

    public void saveEvent(String event,String time,String date,String month,String year)
    {
        myDatabase=new MyDatabase(context);
        SQLiteDatabase database=myDatabase.getWritableDatabase();
        myDatabase.saveEvent(event,time,date,month,year,database);
        myDatabase.close();
        Toast.makeText(context,"Event Saved",Toast.LENGTH_SHORT).show();
    }


    private void InitialLayout()
    {
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.calendar_layout,this);
        nextButton=view.findViewById(R.id.nextButton);
        prevButton=view.findViewById(R.id.prevButton);
        currentDate=view.findViewById(R.id.currentDate);
        gridView=view.findViewById(R.id.gridView);
    }

    private void setCalendar()
    {
        String currntDate=dateFormat.format(calendar.getTime());
        currentDate.setText(currntDate);
        dates.clear();
        Calendar monthCalendar=(Calendar) calendar.clone();
        monthCalendar.set(Calendar.DAY_OF_MONTH,1);
        int FirstDayOfMonth=monthCalendar.get(Calendar.DAY_OF_WEEK)-1;
        monthCalendar.add(Calendar.DAY_OF_MONTH,-FirstDayOfMonth);
        collectEventPerMonth(monthFormat.format(calendar.getTime()),yearFormat.format(calendar.getTime()));
        while(dates.size() < max_cal_days)
        {
            dates.add(monthCalendar.getTime());
            monthCalendar.add(Calendar.DAY_OF_MONTH,1);
        }

        myGridAdapter=new MyGridAdapter(context,dates,calendar,eventList);
        gridView.setAdapter(myGridAdapter);


    }

    private void collectEventPerMonth(String month,String year)
    {
        eventList.clear();
        myDatabase=new MyDatabase(context);
        SQLiteDatabase database=myDatabase.getReadableDatabase();
        Cursor cursor=myDatabase.getEventPerMonth(month,year,database);

        while(cursor.moveToNext())
        {
            String event= cursor.getString(cursor.getColumnIndex(DBStructure.Event));
            String time= cursor.getString(cursor.getColumnIndex(DBStructure.Time));
            String date= cursor.getString(cursor.getColumnIndex(DBStructure.Date));
            String Month= cursor.getString(cursor.getColumnIndex(DBStructure.Month));
            String Year= cursor.getString(cursor.getColumnIndex(DBStructure.Year));
            Events events=new Events(event,time,date,Month,Year);

            eventList.add(events);
        }
        cursor.close();
        myDatabase.close();
    }
}
