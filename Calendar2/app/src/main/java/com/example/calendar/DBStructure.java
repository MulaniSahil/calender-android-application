package com.example.calendar;

public class DBStructure {
    public static final String DBName="EVENTS_DB";
    public static final int DBVersion=1;
    public static final String EventTableName="Events";
    public static final String Event="event";
    public static final String Date="date";
    public static final String Month="month";
    public static final String Year="year";
    public static final String Time="time";

}
